# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix

      # Custom Kernel
      # ./config/kernel.nix

      # Main System Configuration
      ./config/bootloader.nix
      ./config/users.nix
      ./config/security.nix
      ./config/networking.nix

      # Packages
      ./config/packages.nix
      ./config/shell.nix
      ./config/virtualisation.nix
      ./config/sound.nix
      ./config/printing.nix

      # Graphical Backend
      ./config/desktops/xorg.nix
      ./config/desktops/keyboardlayout.nix

      # Desktop interfaces
      # .config/desktops/gnome.nix
      # ./config/desktops/plasma.nix
      ./config/desktops/wayfire.nix
      
    ];

  networking.hostName = "cspark-desktop-nixos"; # Define your hostname.

  # Set your time zone.
  time.timeZone = "Europe/London";

  # System clock might be incorrect after booting Windows and going back to the NixOS. It can be fixed by either setting RTC time standard to UTC on Windows, or setting it to localtime on NixOS.
  # Setting RTC time standard to localtime, compatible with Windows in its default configuration:
  time.hardwareClockInLocalTime = true;

  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "uk";
  };
  
  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?

}

