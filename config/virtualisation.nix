{ config, pkgs, ... }:

{

  # Boot configuration
  boot.kernelParams = [ "amd_iommu=on" "iommu=pt" ];
  # boot.kernelModules = [ "kvm-amd" ];

  # Enable libvirt
  virtualisation.libvirtd = {
    enable = true;
    qemu.ovmf.enable = true;
    qemu.runAsRoot = true;
    qemu.swtpm.enable = true;
    onBoot = "ignore";
    onShutdown = "shutdown";
  };
  environment.sessionVariables.LIBVIRT_DEFAULT_URI = [ "qemu:///system" ];
  programs.dconf.enable = true;

  systemd.services.libvirtd = {
    path = let
             env = pkgs.buildEnv {
               name = "qemu-hook-env";
               paths = with pkgs; [
                 bash
                 libvirt
	         swtpm
                 kmod
                 systemd
                 ripgrep
                 sd
               ];
             };
           in
           [ env ];

    # GPU Passthrough Hooks - If VM is named "windowspassthru" the hooks will be applicable
    preStart = let
      qemuHook = pkgs.writeScript "qemu-hook" ''#!${pkgs.stdenv.shell}

set -x

GUEST_NAME="$1"
OPERATION="$2"
  SUB_OPERATION="$3"

if [ "$GUEST_NAME" == "windowspassthru" ]; then
   # Start VM
   if [ "$OPERATION" == "prepare" ]; then
   # Stop display manager (KDE specific)
   systemctl stop display-manager

   # Unbind VTconsoles
   echo 0 > /sys/class/vtconsole/vtcon0/bind
   echo 0 > /sys/class/vtconsole/vtcon1/bind

   # Unbind EFI-Framebuffer
   echo efi-framebuffer.0 > /sys/bus/platform/drivers/efi-framebuffer/unbind

   # Avoid a race condition
   sleep 5

   # Unload all AMD drivers
   modprobe -r amdgpu

   # Unbind the GPU from display driver
   virsh nodedev-detach pci_0000_08_00_0
   virsh nodedev-detach pci_0000_08_00_1

   # Load VFIO kernel module
   modprobe vfio
   modprobe vfio_pci
   modprobe vfio_iommu_type1

  fi

  # Stop VM
  if [ "$OPERATION" == "release" ]; then
    # Unload VFIO-PCI Kernel Driver
    modprobe -r vfio_pci
    modprobe -r vfio_iommu_type1
    modprobe -r vfio

    # Re-Bind GPU to AMD Driver
    virsh nodedev-reattach pci_0000_08_00_0
    virsh nodedev-reattach pci_0000_08_00_1


    # Rebind VT consoles
    echo 1 > /sys/class/vtconsole/vtcon0/bind
    echo 0 > /sys/class/vtconsole/vtcon1/bind

    nvidia-xconfig --query-gpu-info > /dev/null 2>&1
    # Re-Bind EFI-Framebuffer
    echo "efi-framebuffer.0" > /sys/bus/platform/drivers/efi-framebuffer/bind

    #Loads amd drivers
    modprobe amdgpu
    # Restart Display Manager
    systemctl start display-manager
  fi

fi'';
in ''mkdir -p /var/lib/libvirt/hooks
chmod 755 /var/lib/libvirt/hooks
# Copy hook files
ln -sf ${qemuHook} /var/lib/libvirt/hooks/qemu'';   
};

}
