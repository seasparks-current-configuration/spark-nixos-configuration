{ config, pkgs, ... }:

{

  # Enable doas instead of sudo
  security.doas.enable = true;
  security.sudo.enable = false;
  # Configure doas
  security.doas.extraRules = [{
      users = [ "cspark" ];
      keepEnv = true;
  }];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
      pinentryFlavor = "qt";
  };

  # Enable TPM - TrustedBoot WILL FAIL TO BOOT YOUR SYSTEM if no TPM is available.
  boot.loader.grub.trustedBoot.systemHasTPM = "YES_TPM_is_activated";
  boot.loader.grub.trustedBoot.enable = true;
  security.tpm2 = {
    enable = true;
    abrmd.enable = true;
  };

}
