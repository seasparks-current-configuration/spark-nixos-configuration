{ config, lib, pkgs, pkg-config, flex, ... }:

{
  # boot.kernelPackages = let
  #   spark-linux-pkg = {  stdenv, buildLinux, flex, ... } @ args:

  #     buildLinux (args // rec {
  #       version = "master";
  #       modDirVersion = version;

  #       src = fetchGit {
  #         url = "https://github.com/Jorgsaa/zen-kernel-rdtsc";
  #         rev = "c6c9fbb36562c065c93d5a536690e774eb36d229";
  #       };
  #       kernelPatches = [];

  #       nativeBuildInputs = [
  #         flex
  #       ];

  #       buildInputs = [
  #         flex
  #       ];

  #       #        extraConfig = ''
  #       #            INTEL_SGX y
  #       #          '';

  #       #        extraMeta.branch = "5.4";
  #     } // (args.argsOverride or {}));
  #   spark-linux = pkgs.callPackage spark-linux-pkg{};
  # in
  #   pkgs.recurseIntoAttrs (pkgs.linuxPackagesFor spark-linux);

  # nixpkgs = {
  #   overlays = [
  #     (self: super: {
  #       linux-spark = pkgs.linuxPackagesFor (pkgs.linux.override {
  #         src = fetchGit {
  #           url = "https://gitlab.com/seasparks-current-configuration/torvalds-linux-spark.git";
  #           rev = "d4d7836ea43ca6cf667ebe32b1db1b6bc77916f4";
  #         };
  #         ignoreConfigErrors = true;
  #       });
  #     })
  #   ];
  # };

  # boot.kernelPackages = pkgs.linuxPackagesFor (pkgs.linux_zen.override {
  #   argsOverride = rec {
  #     src = fetchGit {
  #       url = "https://github.com/Jorgsaa/zen-kernel-rdtsc";
  #       rev = "c6c9fbb36562c065c93d5a536690e774eb36d229";
  #     };
  #     ignoreConfigErrors = true;
  #     modDirVersion = "5.14.12-zen-rdtsc";
  #   };
  # });

  boot.kernelPatches = [
    {
      # RDTSC VM Exit Bypass
      name = "vmexit_rdtsc";
      patch = ./custompkgs/vmexit_rdtsc.patch;
    }
  ];

  # boot.kernelPackages = pkgs.linux-spark;

}
