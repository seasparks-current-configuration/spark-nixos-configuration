{ config, pkgs, ... }:

{

  # XDG User Dirs
  environment.etc = {
    "xdg/user-dirs.defaults".text = ''
    DESKTOP=path/relative/to/home
    DOWNLOAD=...
    TEMPLATES=...
    PUBLICSHARE=...
    DOCUMENTS=...
    MUSIC=...
    PICTURES=...
    VIDEOS=...
  '';
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.cspark = {
    isNormalUser = true;
    extraGroups = [ "wheel" "kvm" "input" "libvirtd" "adbusers" ]; # Enable superuser for the user.
    password = "temp";
  };

  users.users.guest = {
    isNormalUser = true;
    extraGroups = [ "audio" ];
    password = "guest";
  };

}
