{}:

with import <nixpkgs> {};

pkgs.writeTextFile {
    name = "spark-engram-xkb-layout";
    destination = "/engram";
    text = ''
default partial // alphanumeric_keys modifier_keys

xkb_symbols "basic" {

    key <TLDE>  { [bracketleft,        braceleft,      notsign,      notsign ] };
    key <AE01>  { [         1,        bar,  onesuperior,   exclamdown ] };
    key <AE02>  { [         2,      equal,  twosuperior,    oneeighth ] };
    key <AE03>  { [         3, asciitilde, threesuperior,    sterling ] };
    key <AE04>  { [         4,       plus,   onequarter,       dollar ] };
    key <AE05>  { [         5,       less,      onehalf, threeeighths ] };
    key <AE06>  { [         6,    greater, threequarters, fiveeighths ] };
    key <AE07>  { [         7, asciicircum,    braceleft, seveneighths ] };
    key <AE08>  { [         8,  ampersand,  bracketleft,    trademark ] };
    key <AE09>  { [         9,    percent, bracketright,    plusminus ] };
    key <AE10>  { [         0,   asterisk,   braceright,       degree ] };
    key <AE11>  { [ bracketright, braceright,    backslash, questiondown ] };
    key <AE12>  { [     slash,  backslash, dead_cedilla,  dead_ogonek ] };

    key <AD01>  { [         b,          B,           at,  Greek_OMEGA ] };
    key <AD02>  { [         y,          Y,      lstroke,      Lstroke ] };
    key <AD03>  { [         o,          O,            e,            E ] };
    key <AD04>  { [         u,          U,    paragraph,   registered ] };
    key <AD05>  { [         apostrophe,          parenleft,       tslash,       Tslash ] };
    key <AD06>  { [         quotedbl,          parenright,    leftarrow,          yen ] };
    key <AD07>  { [         l,          L,    downarrow,      uparrow ] };
    key <AD08>  { [         d,          D,   rightarrow,     idotless ] };
    key <AD09>  { [         w,          W,       oslash,     Ooblique ] };
    key <AD10>  { [         v,          V,        thorn,        THORN ] };
    key <AD11>  { [         z,          Z, dead_diaeresis, dead_abovering ] };
    key <AD12>  { [         numbersign, dollar, dead_tilde,  dead_macron ] };

    key <AC01>  { [         c,          C,           ae,           AE ] };
    key <AC02>  { [         i,          I,       ssharp,      section ] };
    key <AC03>  { [         e,          E,          eth,          ETH ] };
    key <AC04>  { [         a,          A,      dstroke,  ordfeminine ] };
    key <AC05>  { [         comma,          semicolon,          eng,          ENG ] };
    key <AC06>  { [         period,          colon,      hstroke,      Hstroke ] };
    key <AC07>  { [         h,          H,    dead_hook,    dead_horn ] };
    key <AC08>  { [         t,          T,          kra,    ampersand ] };
    key <AC09>  { [         s,          S,      lstroke,      Lstroke ] };
    key <AC10>  { [         n,          N, dead_acute, dead_doubleacute ] };
    key <AC11>  { [         q,          Q, dead_circumflex,  dead_caron ] };
    key <BKSL>  { [        at, asciitilde,   dead_grave,   dead_breve ] };

    key <LSGT>  { [ backslash,        bar,          bar,    brokenbar ] };
    key <AB01>  { [         g,          G, guillemotleft,        less ] };
    key <AB02>  { [         x,          X, guillemotright,    greater ] };
    key <AB03>  { [         j,          J,         cent,    copyright ] };
    key <AB04>  { [         k,          K, leftdoublequotemark, leftsinglequotemark ]   };
    key <AB05>  { [         minus, underscore, rightdoublequotemark, rightsinglequotemark ] };
    key <AB06>  { [         question,  exclam,            n,            N ] };
    key <AB07>  { [         r,          R,           mu,    masculine ] };
    key <AB08>  { [         m,          M, horizconnector,   multiply ] };
    key <AB09>  { [         f,          F, periodcentered,   division ] };
    key <AB10>  { [         p,          P, dead_belowdot, dead_abovedot ] };
};
'';
}
