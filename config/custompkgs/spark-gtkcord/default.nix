{ pkgs, fetchFromGitHub, buildGoModule, makeDesktopItem, lib }:

buildGoModule rec {
	name = "gtkcord";
        version = "master";

        buildInputs = with pkgs; [
		gnome.gtk3
		glib
		gtk-layer-shell
		gdk-pixbuf
		gobjectIntrospection
		libhandy
	];

	nativeBuildInputs = with pkgs; [
		go
		pkgconfig
	];


	src = fetchFromGitHub {
	  owner = "diamondburned";
          repo = "gtkcord3";
          rev = "de6690568a4927e4d0cf46d173fcd596bacced6f";
	  sha256 = "0059harzgdqadvm1jd1bsm2z9jg1s9csp0ps128ifhskg1ykdbcz";
        };

	desktopFile = makeDesktopItem {
		inherit name;
        desktopName = "gtkcord";
		exec = "gtkcord3";
		icon = "gtkcord3";
		categories = "GTK;GNOME;Chat;";
	};

	preFixup = ''
		mkdir -p $out/share/icons/hicolor/256x256/apps/ $out/share/applications/
		# Install the desktop file
		cp "${desktopFile}"/share/applications/* $out/share/applications/
		# Install the icon
		# cp "${src}" $out/share/icons/hicolor/256x256/apps/gtkcord3.png
	'';

        vendorSha256   = "0dfxrcvaz6k9cmkgwx9zpdlyv1hh2iiw27ibgfvj1zm1wacg2l27";
	subPackages = [ "." ];
}
