{}:

with import <nixpkgs> {};

pkgs.writeTextFile {
    name = "spark-wayfire-xorg-session";
    destination = "/share/wayland-sessions/wayfire.desktop";
    text = ''
[Desktop Entry]
Name=Wayfire
Exec=wayfire
TryExec=wayfire
Icon=
Type=Application
DesktopNames=Wayfire
     '';
} // {
    providedSessions = [ "wayfire" ];
}
