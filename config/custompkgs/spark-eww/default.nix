# with import <nixpkgs> {};
{
  pkgs,
  lib,
  pkg-config,
  fetchFromGitHub,
  glib,
  atk,
  cairo,
  pango,
  gdk-pixbuf-xlib,
  gtk3,
  gtkmm3,
  ...
}:

let mkRustPlatform = pkgs.callPackage ./mk-rust-platform.nix {};
    rustPlatform = mkRustPlatform {
      date = "2021-02-08";
      channel = "nightly";
    };
in
rustPlatform.buildRustPackage rec {
  pname = "eww";
  version = "1.0.0";

  src = fetchFromGitHub {
    owner = "elkowar";
    repo = "eww";
    rev = "0aaaa2c2b802dc2015c3a61733f10d5ff0254313";
    sha256 = "1cyg2n0mn166d7z7a08fk8qjkp9dllhj8gs2w3my902smykfq930";
  };

  cargoSha256 = "02js28g5a883q2i5br5v25l46jcanldw8hm2ybljwvbqzl7vj3sg";

  nativeBuildInputs = [
    pkg-config
  ];

  # TODO: remove unused dependencies
  buildInputs = [
    glib
    atk
    cairo
    pango
    gdk-pixbuf-xlib
    gtk3
    gtkmm3
  ];

  meta = with lib; {
    homepage = https://github.com/elkowar/eww;
    description = "ElKowar's wacky widgets";
  };
}
