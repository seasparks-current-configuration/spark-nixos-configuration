{ lib
, stdenv
, fetchFromGitHub

, boca
, smooth
, systemd
, libcdio
, libcdio-paranoia
, libpulseaudio
, expat
, uriparser
, zlib
, coreutils
# , opusTools
}:

stdenv.mkDerivation rec {
  pname = "freac";
  version = "1.1.4";

  src = fetchFromGitHub {
    owner = "enzo1982";
    repo = "freac";
    rev = "v${version}";
    sha256 = "sha256-SP/rVt/8VoeUprwJIIMSIBvoC1Zein3F7MR2tqc2vd0=";
  };

  nativeBuildInputs = [
    coreutils
  ];

  buildInputs = [
    boca
    smooth
    systemd
    libcdio
    libcdio-paranoia
    libpulseaudio
    expat
    uriparser
    zlib
#    opusTools
  ];

  makeFlags = [
    "prefix=$(out)"
  ];

  meta = with lib; {
    description = "The fre:ac audio converter project";
    license = licenses.gpl2Plus;
    homepage = "https://www.freac.org/";
    maintainers = with maintainers; [ shamilton ];
    platforms = platforms.linux;
  };
}
