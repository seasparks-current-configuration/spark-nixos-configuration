self: super:

{
	spark-wayfire-xorg = super.callPackage ./spark-wayfire-xorg {};
	spark-wayfire-xorg-session = super.callPackage ./spark-wayfire-xorg-session {};
	spark-engram-xkb-layout = super.callPackage ./spark-engram-xkb-layout {};
	spark-grapejuice = super.callPackage ./spark-grapejuice {};
	spark-eww = super.callPackage ./spark-eww {};
	spark-freac = super.callPackage ./spark-freac {};
	spark-rofi-wayland = super.callPackage ./spark-rofi-wayland {};
	spark-qutebrowser = super.libsForQt5.callPackage ./spark-qutebrowser {};
	spark-pianoteq-stage = super.callPackage ./spark-pianoteq-stage {};
}
