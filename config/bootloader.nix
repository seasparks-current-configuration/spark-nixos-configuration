{ config, pkgs, ... }:

{

  # EFI Support
  boot.loader.efi.canTouchEfiVariables = true;

  # Plymouth bootscreen
  boot.plymouth.enable = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;

  # boot.loader.grub = {
  #    enable = true;
  #    version = 2;
  #    device = "nodev";
  #    efiSupport = true;
  #    useOSProber = true;
  # 
  #    # This is for an encrypted root, comment out if you do not have that
  #    enableCryptodisk = true;
  # };
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";

  # This is for an encrypted root, replace the uuid with the one of your encrypted root (check via blkid /dev/rootpartitionname or hardware-configuration.nix), or comment out if you do not have that
  #   boot.initrd.luks.devices = [
  #     {
  #       name = "root";
  #       device = "/dev/disk/by-uuid/06e7d974-9549-4be1-8ef2-f013efad727e";
  #       preLVM = true;
  #       allowDiscards = true;
  #     }
  #   ];

}
