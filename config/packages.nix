{ config, pkgs, ... }:

{

  # Enable NonFree/Proprietary packages
  nixpkgs.config.allowUnfree = true;

  # To enable vulkan
  hardware.opengl.driSupport = true;
  # For 32 bit applications
  hardware.opengl.driSupport32Bit = true;

  # Enable NTFS support
  boot.supportedFilesystems = [ "ntfs" ];

  # Add custom packages
  nixpkgs.overlays = [ (import ./custompkgs) ];
  
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [

      # Utilities
      # wineWowPackages.staging
      # (winetricks.override { wine = wineWowPackages.staging; })      
      pciutils
      hwloc
      git      
      nix-prefetch
      nix-prefetch-git      
      go2nix
      appimage-run      
      tpm2-tools
      yadm      
      
      # Keymaps
      xorg.xkbcomp
      spark-engram-xkb-layout # Custom layout - Arno's Engram Keyboard Layout

      # Wayfire
      spark-wayfire-xorg # Wayfire package patched with xorg application support
      spark-wayfire-xorg-session # The session file for starting Wayfire in a login manager
      # spark-eww
      xdg-desktop-portal-wlr
      udiskie
      spark-rofi-wayland # Rofi patched with wayland support
      grim
      slurp
      wlsunset
      swayidle
      swaylock
      swaybg
      mako
      polkit_gnome
      kitty
      imv
      dolphin
      ark      
      pavucontrol
      tree
      pywal
      wpgtk
      qt5ct
      lxappearance
      wdisplays

      # CLI
      youtube-dl
      wget
      vifm-full
      pfetch

      # Programming
      emacs

      # Internet
      spark-qutebrowser
      chromium
      freetube
      
      # Chatting
      discord
      betterdiscordctl
#      spark-gtkcord
      gomuks

      # Games
      runelite
      # multimc
      # spark-grapejuice      

      # Creative
      audacity
      kdenlive
      krita
      gimp
#      obs-studio
#      obs-wlrobs # Wlroots OBS Support
      reaper
      libreoffice
#      spark-pianoteq-stage

      # Media
      zathura
      mpv
      # feh
      cmus
#      spark-freac error: 'AdjustTrackSampleCounts' was not declared in this scope

      # Security
      pinentry-curses
      pinentry-qt
      pass
      qtpass

      # Virtualisation
      virt-manager
      win-virtio
      swtpm	

  ];

  # Enable android debugging tools
  programs.adb.enable = true;  

  # Install Steam
  programs.steam.enable = true;

}
