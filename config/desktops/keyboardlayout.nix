{ config, pkgs, ... }:

{

  # Arno's Engram Keyboard Layout
  services.xserver.extraLayouts = {
     spark-engram = {
        description = "Arno's Engram Keyboard Layout - Packaged by seaspark";
        languages = [ "eng" ];
  	symbolsFile = pkgs.spark-engram-xkb-layout + /engram;
    };
  };

  # Set my default layout to Arno's Engram Keyboard Layout
  services.xserver.layout = "spark-engram";
  # services.xserver.xkbVariant = "spark-engram";

  # Configure keymap in X11
  # services.xserver.layout = "gb";
  # services.xserver.xkbOptions = "eurosign:e";

}
