{ config, pkgs, ... }:

let
  home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/master.tar.gz";
in
{

  # Setup Wayfire Desktop
  services.xserver.displayManager.sessionPackages = [ pkgs.spark-wayfire-xorg-session ];
  services.xserver.displayManager.defaultSession = "wayfire";
  programs.xwayland.enable = true;
  services.xserver.displayManager.sddm.enable = true;

  # Set it in home manager instead
  # Set QT5 theme to get from QT5CT
  environment.sessionVariables = {
#    "CLUTTER_BACKEND" = "wayland";
#    "GDK_BACKEND" = "wayland";
#    "GDK_DPI_SCALE" = "1";
#    "MOZ_ENABLE_WAYLAND" = "1";
#    "QT_QPA_PLATFORM" = "wayland-egl"; # For NVIDIA
#    "QT_QPA_PLATFORM" = "wayland";
    "QT_QPA_PLATFORMTHEME" = "qt5ct";
    "QT_WAYLAND_DISABLE_WINDOWDECORATION" = "1";
#    "SDL_VIDEODRIVER" = "wayland";
    "WLR_NO_HARDWARE_CURSORS" = "1";
    "_JAVA_AWT_WM_NONREPARENTING" = "1";
    "_JAVA_OPTIONS" = "-Dawt.useSystemAAFontSettings=on";
  };

  # Rofi wayland
  # pkgs.rofi.override { package = [ pkgs.rofi-emoji ]; 

  # Qutebrowser add python modules
  # qutebrowser.overrideAttrs (old: {
  #   propagatedBuildInputs = old.propagatedBuildInputs ++ [python39Packages.python-prctl];
  # });


  # Home Manager Config for Wayfire
  imports = [
    (import "${home-manager}/nixos")
  ];

  home-manager.users.cspark = {

    /* Here goes your home-manager config, eg home.packages = [ pkgs.foo ]; */
    #  home.sessionVariables = {
    #  CLUTTER_BACKEND = "wayland";
    #  GDK_BACKEND = "wayland";
    #  GDK_DPI_SCALE = 1;
    #  MOZ_ENABLE_WAYLAND = 1;
    #  # QT_QPA_PLATFORM = "wayland-egl"; # For NVIDIA
    #  QT_QPA_PLATFORM = "wayland";
    #  QT_QPA_PLATFORMTHEME = "qt5ct";
    #  QT_WAYLAND_DISABLE_WINDOWDECORATION = 1;
    #  SDL_VIDEODRIVER = "wayland";
    #  WLR_NO_HARDWARE_CURSORS = 1;
    #  _JAVA_AWT_WM_NONREPARENTING = 1;
    #  _JAVA_OPTIONS = "-Dawt.useSystemAAFontSettings=on";
    # };

    home.packages = [ pkgs.cowsay ];

  };

}
