{ config, pkgs, ... }:

{

  # Enable the Plasma 5 Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.displayManager.plasma5.enable = true;

}
