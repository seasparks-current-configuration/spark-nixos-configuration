{ config, pkgs, ... }:

{

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Make sure Xserver uses the `amdgpu` driver
  services.xserver.videoDrivers = [ "amdgpu" ];

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;

}
