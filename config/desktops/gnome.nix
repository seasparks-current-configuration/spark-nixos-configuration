{ config, pkgs, ... }:

{

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;;

  # For NVIDIA
  # services.xserver.displayManager.gdm.nvidiaWayland = true;

}
